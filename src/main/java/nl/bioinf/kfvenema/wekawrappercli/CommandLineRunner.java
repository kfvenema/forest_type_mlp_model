package nl.bioinf.kfvenema.wekawrappercli;

import java.util.Arrays;

/**
 * CommandLineRunner, the main class of the program
 */
public class CommandLineRunner {
    /**
     * CommandLineRunner, the main object of the Wrapper
     * @param args: The command line arguments
     */
    public static void main(String[] args) {
        try {
            CommandLineOptions op = new CommandLineOptions(args);
            if (op.helpRequested()) {
                op.printHelp();
                return;
            } else {
                MultiLayerPerceptronOptions optionObject = new MultiLayerPerceptronOptions(op.getLearningRate(),
                        op.getMomentum(), op.getSeed(),
                        op.getValidationSetSize(), op.getValidationThreshold(), op.getFilePath(), op.getOutputFormat());
                WekaWrapper wrapper = new WekaWrapper(optionObject);
            }
        } catch (IllegalStateException ex){
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            CommandLineOptions op = new CommandLineOptions(new String[]{});
            op.printHelp();
        }
    }
}
