package nl.bioinf.kfvenema.wekawrappercli;

/**
 * MultiLayerPerceptronOptions: an object that the WekaWrapper needs to function.
 */
public class MultiLayerPerceptronOptions implements OptionsProvider {

    private double learningRate;
    private double momentum;
    private int validationSize;
    private int validationThreshold;
    private int seed;
    private String filePath;
    private String outputFileFormat;

    /**
     *
     * @param learningRate: The learning Rate of the model
     * @param momentum: The momentum of the model
     * @param seed: The seed for the randomizer
     * @param validationSetSize: The validation set size
     * @param validationThreshold: The validation Threshold
     * @param filePath: The filepath of the file that needs classification
     */
    public MultiLayerPerceptronOptions(double learningRate, double momentum,
                                       int seed, int validationSetSize,
                                       int validationThreshold, String filePath, String outputFileFormat){
        this.filePath = filePath;
        this.learningRate = learningRate;
        this.momentum = momentum;
        this.validationSize = validationSetSize;
        this.validationThreshold = validationThreshold;
        this.seed = seed;
        this.outputFileFormat = outputFileFormat;
    }

    public double getLearningRate(){
        return this.learningRate;
    }

    public double getMomentum(){
        return this.momentum;
    }

    public int getSeed(){
        return this.seed;
    }

    public int getValidationSetSize() {
        return this.validationSize;
    }

    public int getValidationThreshold() {
        return this.validationThreshold;
    }

    public String getFilePath() {
        return this.filePath;
    }

    public String getOutputFormat() {
        return this.outputFileFormat;
    }
}
