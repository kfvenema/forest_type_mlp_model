/*
 * Copyright (c) 2018 Rick Venema
 * All right reserved
 * www.bioinf.nl
 */

package nl.bioinf.kfvenema.wekawrappercli;

import org.apache.commons.cli.*;

/**
 * Object containing all the commandline arguments
 */
public class CommandLineOptions implements OptionsProvider{
    private static final String HELP = "help";
    private static final String LEARNINGRATE = "learningrate";
    private static final String MOMENTUM = "momentum";
    private static final String VALIDATIONSIZE = "validationsize";
    private static final String VALIDATIONTHRESHOLD = "validationthreshold";
    private static final String SEED = "seed";
    private static final String FILEPATH = "file";
    private static final String OUTPUTFILEFORMAT = "outputfileformat";

    private Options options;
    private double learningRate;
    private double momentum;
    private int validationSize;
    private int validationThreshold;
    private int seed;
    private String filePath;

    private CommandLine commandLine;
    private final String[] commandLineArgs;
    private String outputFileFormat;

    /**
     * CommandLineOptions: the initialize of the commandline options
     * @param args: The Command line arguments
     */
    public CommandLineOptions(final String[] args) {
        this.commandLineArgs = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        parseArguments();
    }

    /**
     * check if help was requested; if so, return true.
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * builds the Options objects
     */
    private void buildOptions() {
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");
        Option learningRateOption = new Option("L", LEARNINGRATE, true,
                "Sets the learning rate, default = 0.3");
        Option momentumOption = new Option("M", MOMENTUM, true, "Sets the momentum rate for the algorithm");
        Option validationSizeOption = new Option("V", VALIDATIONSIZE, true, "Sets the percentage size of validation set to use to terminate training");
        Option validationThresholdOption = new Option("E", VALIDATIONTHRESHOLD, true, "Sets the consequetive number of errors allowed for validation");
        Option seedOption = new Option("S", SEED, true, "Sets the seed for the random number generator");
        Option fileOption= new Option("F", FILEPATH, true, "Sets the filepath");
        Option outputFileFormat = new Option("OUT", OUTPUTFILEFORMAT, true, "Sets the output file format");

        options.addOption(helpOption);
        options.addOption(learningRateOption);
        options.addOption(momentumOption);
        options.addOption(validationSizeOption);
        options.addOption(validationThresholdOption);
        options.addOption(seedOption);
        options.addOption(fileOption);
        options.addOption(outputFileFormat);
    }

    /**
     * Function to parse the arguments of the CLI
     */
    private void parseArguments(){
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.commandLineArgs);

            //parse the args
            if (this.commandLine.hasOption(FILEPATH)) {
                String arg = commandLine.getOptionValue(FILEPATH).trim();
                this.filePath = arg;
            } else {
                return;
            }

            if (this.commandLine.hasOption(LEARNINGRATE)){
                String arg = commandLine.getOptionValue(LEARNINGRATE).trim();

                this.learningRate = Double.parseDouble(arg);
            } else {
                this.learningRate = 0.3;
            }

            if (this.commandLine.hasOption(MOMENTUM)){
                String arg = commandLine.getOptionValue(MOMENTUM).trim();
                this.momentum = Double.parseDouble(arg);
            } else {
                this.momentum = 0.2;
            }

            if (this.commandLine.hasOption(VALIDATIONSIZE)){
                String arg = commandLine.getOptionValue(VALIDATIONSIZE).trim();
                this.validationSize = Integer.parseInt(arg);
            } else {
                this.validationSize = 0;
            }

            if (this.commandLine.hasOption(VALIDATIONTHRESHOLD)){
                String arg =commandLine.getOptionValue(VALIDATIONTHRESHOLD).trim();
                this.validationThreshold = Integer.parseInt(arg);
            } else {
                this.validationThreshold = 20;
            }

            if (this.commandLine.hasOption(SEED)){
                String arg = commandLine.getOptionValue(SEED).trim();
                this.seed = Integer.parseInt(arg);
            } else {
                this.seed = 0;
            }
            if (this.commandLine.hasOption(OUTPUTFILEFORMAT)) {
                String arg = commandLine.getOptionValue(OUTPUTFILEFORMAT);
                this.outputFileFormat = arg;
            } else {
                this.outputFileFormat = "arff";
            }
        } catch (ParseException ex) {
            System.err.println("Wrong arguments given");
        }
    }

    /**
     * Function to print the help
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Multilayer Perceptron command line tool", options);
    }
    @Override
    public double getLearningRate() {
        return this.learningRate;
    }

    @Override
    public double getMomentum() {
        return this.momentum;
    }

    @Override
    public int getSeed() {
        return this.seed;
    }

    @Override
    public int getValidationSetSize() {
        return this.validationSize;
    }

    @Override
    public int getValidationThreshold() {
        return this.validationThreshold;
    }

    @Override
    public String getFilePath() {
        return this.filePath;
    }

    @Override
    public String getOutputFormat() {
        return this.outputFileFormat;
    }
}
