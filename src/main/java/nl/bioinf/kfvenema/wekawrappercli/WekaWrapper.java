package nl.bioinf.kfvenema.wekawrappercli;

import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;
import weka.core.converters.ConverterUtils;

import java.io.File;
import java.io.IOException;

public class WekaWrapper {
    private final String modelFile = "./data/MLP.model";
    private final String datafile = "./data/CompleteTrainingsData.arff";
    private SupportedFileType fileType;



    /**
     * WekaWrapper, starts the program with the given options via an MultiLayerPerceptronOptions
     * @param options: An OptionsPrividerObject, which include the necessary parameters
     */
    public WekaWrapper(MultiLayerPerceptronOptions options){
        start(options);
    }

    /**
     * Starts the weka wrapper
     * @param options: An OptionsPrividerObject, which include the necessary parameters
     */
    private void start(MultiLayerPerceptronOptions options) {

        String unknownFile = options.getFilePath();

        try {
            fileExist(new File(unknownFile));
            fileExist(new File(this.datafile));
            Instances instances = loadArff(this.datafile);
            MultilayerPerceptron mlp = buildClassifier(instances, options);
            saveClassifier(mlp);
            MultilayerPerceptron fromFile = loadClassifier();

            this.fileType = getFileType(new File(unknownFile));

            Instances unknownInstances = loadInstances(unknownFile, this.fileType);

            Instances newLabeled = classifyNewInstance(fromFile, unknownInstances);
            writeLabeled(newLabeled, options);
            // classifyNewInstance(fromFile, unknownInstances);

        } catch (NullPointerException nullPointerException){
            System.err.println("It seems something is empty, are you sure everything is correct?");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  Function to check if a file exists
     * @param file The file that needs to be checked
     * @throws IOException
     * @throws NullPointerException
     */
    private void fileExist(File file) throws IOException, NullPointerException{
        if (file != null && file.exists()){

        } else {
            throw new IOException("File does not exist: " + file);
        }
    }

    /**
     *  Load the different instances from the file that needs classification
     * @param unknownFile The file with the unknown instances
     * @param unknownFileExtension The extension of the file that is given to the function
     * @return
     * @throws IOException
     */
    private Instances loadInstances(String unknownFile, SupportedFileType unknownFileExtension) throws IOException {
        Instances unknownInstances = null;

        switch (unknownFileExtension) {
            case ARFF:
                unknownInstances = loadArff(unknownFile);
                break;
            case CSV:
                unknownInstances = loadCSV(unknownFile);
                break;
        }
        return unknownInstances;
    }

    /**
     * Gets the file type of the given file
     * @param file The file of which the file type is needed
     * @return The type of the file that has been passed
     */
    private static SupportedFileType getFileType(File file) {
        String extension;
        SupportedFileType type;

        String name = file.getName();
        extension = name.substring(name.lastIndexOf("."));
        type = SupportedFileType.fromExtension(extension);

        return type;
    }

    /**
     * Function to write the classified instances to a file
     * @param newLabeled The output that needs to be written to file
     * @param options The options provider object to get the outputformat of the file
     */
    private void writeLabeled(Instances newLabeled, OptionsProvider options) {
        try {
            SupportedFileType type;
            type = SupportedFileType.fromExtension("." + options.getOutputFormat());
            switch (type) {
                case ARFF:
                    writeARFF(newLabeled);
                case CSV:
                    writeCSV(newLabeled);
            }
        } catch (IOException e){
            System.err.println("Something went wrong with writing to output file!");
        }
    }


    /**
     * Function to write the instances to ARFF file type
     * @param newLabeled The classified instances that needs to be written to file
     * @throws IOException
     */
    private void writeARFF(Instances newLabeled) throws IOException {
        ArffSaver saver = new ArffSaver();
        saver.setInstances(newLabeled);
        saver.setFile(new File("data/out.arff"));
        saver.writeBatch();
    }

    /**
     * Function to write the instances to CSV file type
     * @param newLabeled The classified instances that neees to be written to file
     * @throws IOException
     */
    private void writeCSV(Instances newLabeled) throws IOException{
        CSVSaver saver = new CSVSaver();
        saver.setInstances(newLabeled);
        saver.setFile(new File("data/out.csv"));
        saver.writeBatch();
    }

    /**
     * Function to classify the different instances from the given model
     * @param tree The multilayer perceptron object
     * @param unknownInstances the unknown instances that needs to be classified
     * @return the labeled instances
     * @throws Exception
     */
    private Instances classifyNewInstance(MultilayerPerceptron tree, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = tree.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        return labeled;
    }

    /**
     * Function that loads the model file of the multilayerperceptron
     * @return the multilayer perceptron model from the file
     * @throws Exception
     */
    private MultilayerPerceptron loadClassifier() throws Exception{
        return (MultilayerPerceptron) weka.core.SerializationHelper.read(modelFile);
    }

    /**
     * Function that can load the different instances from an ARFF file
     * @param datafile the file that needs to be read
     * @return the data that is in the given ARFF file
     * @throws IOException
     */
    private Instances loadArff(String datafile) throws IOException {
        try {
            ConverterUtils.DataSource source = new ConverterUtils.DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file: " + datafile);
        }
    }

    /**
     * Function to read the data from a csv file
     * @param unknownFile the file that needs to be read
     * @return the data that is in the given CSV file
     * @throws IOException Exception if something is wrong with the file
     */
    private Instances loadCSV(String unknownFile) throws IOException{
        Instances inst;
        try {
            CSVLoader csv = new CSVLoader();
            csv.setFile(new File(unknownFile));
            inst = csv.getDataSet();
        } catch (Exception e) {
            throw new IOException("could not read from file: " + unknownFile);
        }
        return inst;
    }

    /**
     * Function to save the classifier
     * @param mlp the multilayer perceptron
     * @throws Exception
     */
    private void saveClassifier(MultilayerPerceptron mlp) throws Exception {
        //post 3.5.5
        // serialize model
        weka.core.SerializationHelper.write(modelFile, mlp);
    }

    /**
     * Function that builds the Classifier
     * @param instances the instances that are needed to build the model
     * @param options the options that are needed to build the classifier
     * @return the finalized model
     * @throws Exception
     */
    private MultilayerPerceptron buildClassifier(Instances instances, MultiLayerPerceptronOptions options) throws Exception{
        MultilayerPerceptron mlp = new MultilayerPerceptron();
        mlp.setLearningRate(options.getLearningRate());
        mlp.setMomentum(options.getMomentum());
        mlp.setValidationThreshold(options.getValidationThreshold());
        mlp.setValidationSetSize(options.getValidationSetSize());
        mlp.setSeed(options.getSeed());

        mlp.buildClassifier(instances);
        return mlp;
    }
}
