/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.kfvenema.wekawrappercli;


/**
 * interface that specifies which options should be provided to the tool.
 * @author Rick Venema
 */
public interface OptionsProvider {
    /**
     * serves the learning rate of the wrapper
     * @return learningRate the learning rate
     */
    double getLearningRate();

    /**
     * serves the momentum of the wrapper
     * @return momentum the momentum
     */
    double getMomentum();

    /**
     * serves the seed of the wrapper
     * @return seed the seed of the wrapper
     */
    int getSeed();

    /**
     * serves the validationset size
     * @return validationSetSize
     */
    int getValidationSetSize();

    /**
     * serves the Validation Thresholds
     * @return validationThreshold
     */
    int getValidationThreshold();

    /**
     * serves the File path
     * @return filePath
     */
    String getFilePath();

    /**
     * serves the Output Format
     * @return the outputFormat
     */
    String getOutputFormat();
}
