package nl.bioinf.kfvenema.wekawrappercli;

public enum SupportedFileType {
    CSV,
    ARFF,
    UNKNOWN;

    /**
     * SupportedFileType form Extension,
     * @param extension
     * @return
     */
    public static SupportedFileType fromExtension(String extension) {
        extension = extension.toLowerCase();
        switch(extension) {
            case ".csv":
                return SupportedFileType.CSV;
            case ".arff":
                return SupportedFileType.ARFF;
            default:
                return SupportedFileType.UNKNOWN;
        }
    }
}
