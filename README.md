# Forest Type MLP classification model
A Japanese forest classification program that can classify the different tree types in forests

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

For this project you will need Weka, java 10, and this git repo 
```
git clone https://github.com/RickVenema/Forest_Type_MLP_Model.git
```

### Installing
#### Working on the source code
To install this wonderful program the following need to be done:

First download this repository, which you have obviously done because it was in the prerequisites...

Second: open this project in inteliJ and run the build.gradle, if correctly, this will also install all dependencies that are needed 
to run the program. 

Third: The main class of the program is `CommandLineRunner.java` run this main to get the program working, but you need to configure your parameters
first in the menu. Don't know how, follow [this](https://www.jetbrains.com/help/idea/run-debug-configuration-application.html) tutorial.

Fourth: profit, you now have my program working on your machine. Having problems, just contact me via k.f.venema@st.hanze.nl

`-h` in the parameters will get you all the possibilities this program has, and more. 
#### But I only want to run your program!
That is fine too, the first step to this is the exact same, just as all the other steps. The only difference is the fact that the .jar file 
needs to be build first in order for you to run the program. This is not yet available, and will be added as soon as I will get it working. 




## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Weka](https://cs.waikato.ac.nz/ml/weka) - Machine learning algorithms platform based on java

## Authors

* **Rick Venema** - *Initial work* - [RickVenema](https://github.com/RickVenema)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the GLPv3 License - see the [LICENSE.md](LICENSE.md) file for details
